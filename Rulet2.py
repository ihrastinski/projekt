#---------------------------------------------------------#
#..........Odjel za informatiku Sveučilišta u Rijeci......#
#..........Kolegij: Distribuirani sustavi.................#
#..........Tema: MonteCarlo simulacija ruleta.............#
#..........Tim: Ivan Hrastinski, Ivan Žufić...............#
#---------------------------------------------------------#

import numpy as np
from mpi4py import MPI
import sys
import random
from time import time

def colorTest(n):
    """
    Vraća broj na temelju kojeg se određuje
    boja polja. Broj 0:zeleno polje, 1:crno
    polje, 2:crveno polje
    
    Argumenti:
    n -- broj na temelju kojeg određuje boju,
    odnosno broj polja u ruletu.
    """
    if n == 0:
        return 0
    elif n>0 and n< 11:
        if n%2 == 0:
            return 1
        else:
            return 2
    elif n>10 and n<19:
        if n%2 == 0:
            return 2
        else:
            return 1
    elif n>18 and n< 29:
        if n%2 == 0:
            return 1
        else:
            return 2
    elif n>28 and n<37:
        if n%2 == 0:
            return 2
        else:
            return 1

def playerTest(playersNum,playerType,playerTry,playerMoney,hitNumber,houseMoney):
    """
    Određuje vrijednost sljedećih polja: playerTry, playerMoney, houseMoney.
    
    Argumenti:
    playersNum -- broj igrača ruleta
    playerType -- 1D polje s tipovima igrača, index+1 polja je redni broj 
                  igrača u ruletu, a broj tipa igrača ruleta(0-ulaže 
                  na sve, 1-ulaže na boju, 2-ulaže na broj) se
                  upisuje u polje s određenim index-om igrača
    playerTry -- 1D polje s pokušajima igrača, index+1 polja je redni broj igrača 
                 u polju, a broj ili boja koju odabire igrač se upisuje u polje
                 sukladno s njegovim index-om, tj polje na  želi uložiti novac 
                 (0-36 polja ruleta, 37-39 boje u ruletu)
    playerMoney -- 2D polje s novcem i ulogom igrača, playerMoney[0,igrač-1]-novac 
                   playerMoney[1,igrač-1]-ulog igrača
    hitNumber -- broj koji je izbacio rulet
    houseMoney -- polje u kojem se nalazi novac kuće
    """
    #postavljanje uloga i broja ili boje na koje igrač ulaže novac
    for h in range(0, playersNum[0]):
        if playerMoney[0,h] > 0:
            if playerType[h] == 0:
                playerTry[h] = random.randrange(0,40) #37-zelena  38-crna  39-crvena
            elif playerType[h] == 1:
                playerTry[h] = random.randrange(37,40)
            elif playerType[h] == 2:
                playerTry[h] = random.randrange(0,37)
            playerMoney[1,h] = random.randrange(0,playerMoney[0,h])
            playerMoney[0,h] -= playerMoney[1,h]

    #provjera da li je igrač pogodio i upravljanje novcem
    for t in range(0, playersNum[0]):
        if playerType[t] == 0:
            if playerTry[t] == hitNumber:
                playerMoney[0,t] += playerMoney[1,t]*35
                houseMoney[0] -= playerMoney[1,t]*35 - playerMoney[1,t]         
            elif playerTry[t] == colorTest(hitNumber)+37:
                playerMoney[0,t] += playerMoney[1,t]*2 
                houseMoney[0] -= playerMoney[1,t]*2 - playerMoney[1,t]
            else:
                houseMoney[0] += playerMoney[1,t]
        elif playerType[t] == 1:
            if playerTry[t] == colorTest(hitNumber)+37:
                playerMoney[0,t] += playerMoney[1,t]*2
                houseMoney[0] -= playerMoney[1,t]*2 - playerMoney[1,t]
            else:
                houseMoney[0] += playerMoney[1,t]
        elif playerType[t] == 2:
            if playerTry[t] == hitNumber:
                playerMoney[0,t] += playerMoney[1,t]*35
                houseMoney[0] -= playerMoney[1,t]*35 - playerMoney[1,t]
            else:
                houseMoney[0] += playerMoney[1,t]

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

#novac kuće
houseMoney = np.empty(1, dtype=np.int64)
houseMoney[0] = 1000000

if rank == 0:
    sumCountNumbers = np.zeros(37, dtype=np.int32)

    #broj igrača
    flag = True
    while (flag == True):
         try:
             players=int(input("Unesi broj igrača(1-8): "))
         except:
             players = 0
         if players >= 1 and players <= 8:
             flag = False

    playersNum = np.zeros(1,dtype=np.int32)       
    playersNum[0] = players

    #vrijednost tipa igrača 0-može ulagati na sve  1-može ulagati na boju  2-može ulagati na broj
    playerType = np.zeros(players, dtype=np.int32)

    #broj igrača koji mogu ulagati na sve
    flagAll = True
    while (flagAll == True):
        try:
            print("Broj igrača koji mogu ulagati na sve(1-{0}):".format(players))
            playersAll=int(input())
        except:
            playersAll = 0
        if playersAll >= 1 and playersAll <= players:
            flagAll = False

    for xx in range(0,playersAll):
        playerType[xx] = 0

    #broj igrača koji mogu ulagati samo na boju
    if players - playersAll > 0:
        flagColor = True
        while (flagColor == True):
            try:
                print("Broj igrača koji mogu ulagati na boju(1-{0}):".format(players-playersAll))
                playersColor=int(input())
            except:
                playersColor = 0
            if playersColor >= 1 and playersColor <= players-playersAll:
                flagColor = False
        for yy in range(0,playersColor):
            playerType[yy+playersAll] = 1

        #broj igrača koji mogu ulagati samo na broj
        if players - playersAll-playersColor > 0:
            playersNumber = players - playersAll-playersColor
            for zz in range(0,playersNumber):
                playerType[zz+playersAll+playersColor] = 2

    #novac i ulog igrača
    print("\n--Početni kapital--")
    playerMoney = np.zeros((2,players), dtype=np.int64)
    playerMoneyReduced = np.empty((2,players),dtype=np.int64)
    for m in range(0,players):
        playerMoney[0,m] = random.randrange(500,1000)
        print("Igrač", m+1, "ima", playerMoney[0,m],"kn")
    
    print("Kuća ima ",houseMoney[0],"kn\n")
    tempsum = np.sum(playerMoney)
    colorCountAll = np.zeros(3, dtype=np.int32)
    houseMoneyReduced = np.empty(1,dtype=np.int64)
else:
    playersNum = np.zeros(1,dtype=np.int32)
    sumCountNumbers = None
    colorCountAll = None
    playerMoneyReduced = None
    houseMoneyReduced = None

# indeks: 0-zeleno polje, 1-crno polje, 2-crveno polje
colorCount = np.zeros(3, dtype=np.int32)

#slanje broja igrača svim procesima
comm.Bcast(playersNum, root=0)

if rank != 0:
    playerType = np.empty(playersNum[0], dtype=np.int32)
    playerMoney = np.zeros((2,playersNum[0]), dtype=np.int64)

#slanje tipa igrača i novca svim procesima
comm.Bcast(playerType, root = 0)
comm.Bcast(playerMoney, root = 0)
   
try:
    nIterations = int(sys.argv[1])
except:
    nIterations = 20

iterationsPart = int(nIterations/size)
iterationsRest = nIterations%size

playerTry = np.zeros(playersNum[0], dtype=np.int32)
hitNumbers = np.zeros(nIterations, dtype=np.int32)
countHitNumbers = np.zeros(37, dtype=np.int32)

start = time()
#generiranje slučajnog broja i poziv funkcija colorCount i playerTest
for i in range(0, iterationsPart):         
    hitNumbers[i+(iterationsPart*rank)] = random.randrange(0,37)
    colorCount[colorTest(hitNumbers[i+(iterationsPart*rank)])] +=1
    playerTest(playersNum,playerType,playerTry,playerMoney,hitNumbers[i+(iterationsPart*rank)],houseMoney)

#ako se broj iteracija podjeli s brojem procesa te ako je ostatak veći,
#od 0, generiranje slučajnog broja i poziv funkcija colorCount i playerTest
#za taj ostatak    
if iterationsRest > 0 and rank == size-1:
    for j in range(0, iterationsRest):
        hitNumbers[j+iterationsPart*rank+iterationsPart]=random.randrange(0,37)
        colorCount[colorTest(hitNumbers[j+iterationsPart*rank+iterationsPart])] +=1
        playerTest(playersNum,playerType,playerTry,playerMoney,hitNumbers[j+iterationsPart*rank+iterationsPart], houseMoney)

#novac igrača-računanje prosječne vrijednosti
for pla in range(0, playersNum[0]):
    playerMoney[0,pla] = playerMoney[0,pla]/size

#novac kuće-računanje prosjčne vrijednosti    
houseMoney[0]=houseMoney[0]/size

#Prebrojavanje slučajnih brojeva u niz, polje u nizu s indeksom 0 je polje 0 u ruletu
#tako sve do 36         
for z in range(0, iterationsPart):
    countHitNumbers[hitNumbers[z+(iterationsPart*rank)]] += 1

if iterationsRest > 0 and rank == size-1:
    for y in range(0, iterationsRest):
        countHitNumbers[hitNumbers[y+iterationsPart*rank+iterationsPart]] += 1 

#redukcija slučajnih boja i brojeva
comm.Reduce(colorCount, colorCountAll, op=MPI.SUM, root=0)
comm.Reduce(countHitNumbers, sumCountNumbers, op=MPI.SUM, root=0)
#redukcija novca igrača i novca kuće
comm.Reduce(playerMoney, playerMoneyReduced, op=MPI.SUM, root=0)
comm.Reduce(houseMoney, houseMoneyReduced, op=MPI.SUM, root=0)

end = time()

p = 0
if rank == 0:
    for r in sumCountNumbers:
        print("Polje", p," ", np.round(r/nIterations*100,2),"%")
        p += 1
    for c in range(3):
        if c == 0:
            print("\nZeleno polje ",np.round(colorCountAll[c]/nIterations*100,2),"%")
        elif c == 1:
            print("Crna polja ",np.round(colorCountAll[c]/nIterations*100,2),"%")
        elif c == 2:
            print("Crvena polja ",np.round(colorCountAll[c]/nIterations*100,2),"%")

    print("\n--Nakon", nIterations, "pokušaja prosječni iznos na",size,"stola--\n")
    for pl in range(0,playersNum[0]):
        print("Igrač", pl+1, "ima", playerMoneyReduced[0,pl],"kn")
    
    print("Kuća ima",houseMoneyReduced[0],"kn")
    print("\nVrijeme izvođenja simulacije iznosi",np.round(end-start,5),"s")
