#---------------------------------------------------------
#..........Odjel za infromatiku Sveučilišta u Rijeci......
#..........Kolegij: Distribuirani sustavi.................
#..........Tema: MonteCarlo simulacija ruleta.............
#..........Tim: Ivan Hrastinski, Ivan Žufić...............
#---------------------------------------------------------

import numpy as np
from mpi4py import MPI
import sys
import random
from termcolor import cprint, colored

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

try:
    nIterations = int(sys.argv[1])
except:
    nIterations = 10

iterationsPart = int(nIterations/size)
iterationsRest = nIterations%size

hitNumbers = np.zeros(nIterations, dtype=np.int32)
countHitNumbers = np.zeros(37, dtype=np.int32)

#Dodavanje slučajnih brojeva u niz      
for i in range(0, iterationsPart):    
    hitNumbers[i+(iterationsPart*rank)] = random.randrange(0,37)

if iterationsRest > 0 and rank == size-1:
    for j in range(0, iterationsRest):
        hitNumbers[j+iterationsPart*rank+iterationsPart]=random.randrange(0,37)

#Prebrojavanje slučajnih brojeva u niz, polje u nizu s indeksom 0 je polje 0 u ruletu
#tako sve do 36         
for z in range(0, iterationsPart):
    countHitNumbers[hitNumbers[z+(iterationsPart*rank)]] += 1

if iterationsRest > 0 and rank == size-1:
    for y in range(0, iterationsRest):
        countHitNumbers[hitNumbers[y+iterationsPart*rank+iterationsPart]] += 1 

if rank == 0:
    sumCountNumbers = np.zeros(37, dtype=np.int32)
    # indeks: 0-zeleno polje, 1-crno polje, 2-crveno polje
    colorCount = np.zeros(3, dtype=np.int32)
else:
    sumCountNumbers = None

comm.Reduce(countHitNumbers, sumCountNumbers, op=MPI.SUM, root=0)

p = 0
if rank == 0:
    print("Rulet -- MonteCarlo simulacija\nBroj pokušaja",nIterations,"\n")
    #Prebrojavanje polja po bojama
    for r in sumCountNumbers:
        if p == 0:
            colorCount[0] += 1
        elif p>0 and p< 11:
            if r%2 == 0:
                colorCount[1] += 1
            else:
                colorCount[2] += 1
        elif p>10 and p<19:
            if r%2 == 0:
                colorCount[2] += 1
            else:
                colorCount[1] += 1
        elif p>18 and p< 29:
            if r%2 == 0:
                colorCount[1] += 1
            else:
                colorCount[2] += 1
        elif p>28 and p<37:
            if r%2 == 0:
                colorCount[2] += 1
            else:
                colorCount[1] += 1
        print("Polje", p," ", np.round(r/nIterations*100,2),"%")
        p += 1
    for c in range(3):
        if c == 0:
            print("\nZeleno polje ",np.round(colorCount[c]/37*100,2),"%")
        elif c == 1:
            print("Crna polja ",np.round(colorCount[c]/37*100,2),"%")
        elif c == 2:
            print("Crvena polja ",np.round(colorCount[c]/37*100,2),"%")
       	


    
        

